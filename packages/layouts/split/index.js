import React from 'react'
import injectStyles from 'react-jss'

const Split = ({classes, children}) => <div className={classes.wrapper}>
  {React.Children.map(children, x => <div className={classes.item}>{x}</div>)}
</div>

const styles = {
  wrapper: {
    display: 'flex',
    flexDirection: 'row'
  },
  item: {
    flex: 1,
    margin: '1em'
  }
}

export default injectStyles(styles)(Split)

