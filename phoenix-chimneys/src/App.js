import React, { Component } from 'react'
import './App.css'
import { Split } from '@alan/layouts'
import { Card } from '@alan/components'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Split>
          <Card />
          <Card />
          <Card />
        </Split>
      </div>
    );
  }
}

export default App;
