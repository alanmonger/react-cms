import React from 'react'
import ReactDOM from 'react-dom'
import add from 'add'

class App extends React.Component {
  state = {
    numberOne: 0,
    numberTwo: 0
  }

  increment = (ref) => () => {
    this.setState({
      [ref]: this.state[ref] + 1
    })
  }

  render() {
    return <div>
      <button onClick={this.increment('numberOne')}>+</button> {this.state.numberOne}
      <button onClick={this.increment('numberTwo')}>+</button> {this.state.numberTwo}

      {add(this.state.numberOne, this.state.numberTwo)}
    </div>
  }
}

export default App
