import add from 'add'
import subtract from 'subtract'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App.js'

let mount = document.createElement('div')
ReactDOM.render(<App />, mount)
document.getElementsByTagName('body')[0].appendChild(mount)
